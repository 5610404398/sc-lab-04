import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class ViewSystem {
	public ViewSystem() {
	}

	public void runGui() {
		MainSystem main = new MainSystem();
		JFrame jf = new JFrame("N E S T E D    L O O P ");
		jf.setDefaultCloseOperation(jf.EXIT_ON_CLOSE);
		jf.setSize(750, 450);
		jf.setLayout(null);

		// ANSWER WINDOW
		JPanel panel1 = new JPanel();
		panel1.setBounds(20, 20, 250, 380);
		panel1.setBackground(Color.LIGHT_GRAY);
		panel1.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		panel1.setLayout(null);

		// MANAGE WINDOW
		JPanel panel2 = new JPanel();
		panel2.setBackground(Color.WHITE);
		panel2.setBorder(BorderFactory.createLineBorder(Color.black));
		panel2.setBounds(300, 20, 420, 380);
		panel2.setLayout(null);

		JComboBox box = new JComboBox();
		box.setBackground(Color.WHITE);
		box.setBounds(25, 50, 200, 30);
		box.addItem("Output 1");
		box.addItem("Output 2");
		box.addItem("Output 3");
		box.addItem("Output 4");
		

		// Create Textfield
		JTextField number = new JTextField();
		number.setBorder(BorderFactory.createLineBorder(Color.black));
		number.setBounds(25, 140, 200, 25);

		JTextArea ans = new JTextArea();
		ans.setBounds(10, 10, 400, 360);

		// Create text label

		JLabel type = new JLabel("SELECT TYPE OF OUTPUT :: ");
		type.setBounds(10, 20, 200, 25);

		JLabel num = new JLabel("INPUT NUMBER ::");
		num.setBounds(10, 110, 200, 25);

		// Create Button
		JButton ok = new JButton(" O K ");
		ok.setBackground(Color.pink);
		ok.setBounds(75, 250, 100, 25);

		class AddInterestListener implements ActionListener {
			public void actionPerformed(ActionEvent event) {
				String name = (String) box.getSelectedItem();
				String numbernum = number.getText();
				int numm = Integer.parseInt(numbernum);
				String show = main.runControllerNtl(name, numm);
				ans.setText(show);
				
			}
		}

		ActionListener listener = new AddInterestListener();
		ok.addActionListener(listener);

		// add item

		jf.add(panel1);
		jf.add(panel2);

		panel1.add(box);
		panel1.add(type);
		panel1.add(num);
		panel1.add(ok);
		panel1.add(number);

		panel2.add(ans);
		jf.setVisible(true);
		jf.setResizable(false);
	}

}
