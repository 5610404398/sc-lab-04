import javax.swing.JTextArea;
public class NestedLoop {
	int i;
	int j;
	String s;
	String all; 

	public NestedLoop() {
		this.s = "";
		this.all = "";
	}

	public String nlOutput1(int n) {
		for (i = 1; i <= n; i++) {
			for (j = 1; j <= n + 1; j++) {
				s += "*";
			}
			all += s ;
			s = "\n";
		}
		return all ;}

	public String nlOutput2(int n) {
		for (i = 1; i <= n + 1; i++) {
			for (j = 1; j <= i; j++) {
				s += "*";
			}
			all += s ;
			s = "\n";
		}
		return all;
	}

	public String nlOutput3(int n) {
		for (i = 1; i <= n; i++) {
			for (j = 1; j <= n + 2; j++) {
				if (j % 2 == 0) {
					s += "*";
				} else {
					s += "-";
				}
			}
			all += s ;
			s = "\n";
		}
		return all;
	}

	public String nlOutput4(int n) {
		for (i = 1; i <= n; i++) {
			for (j = 1; j <= n + 2; j++) {
				if ((i + j) % 2 == 0) {
					s += "*";
				} else {
					s += " ";
				}
			}
			all += s ;
			s = "\n";
		}
		return all;
	}

	public String inputController(String type, int num) {
		all = "";
		if (type == "Output 1") {
			String all = nlOutput1(num);
			
		} else if (type == "Output 2") {
			String all = nlOutput2(num);
		} else if (type == "Output 3") {
			String all = nlOutput3(num);
		} else {
			String all = nlOutput4(num);
		}
		return all;
	}
}
